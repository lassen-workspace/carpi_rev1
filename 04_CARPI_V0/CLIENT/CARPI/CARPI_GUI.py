from PyQt5 import QtGui
from PyQt5.QtWidgets import QWidget, QApplication, QLabel, QVBoxLayout, QSlider, QPushButton
from PyQt5.QtGui import QPixmap
import sys
import cv2
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt, QThread
import numpy as np
import socket
import time

# SERVER IP ADDRESS
s = socket.socket()
hostname = '192.168.1.25' #Server IP/Hostname
port = 8080 #Server Port

class VideoThread(QThread):
    change_pixmap_signal = pyqtSignal(np.ndarray)

    def __init__(self):
        super().__init__()
        self._run_flag = True

    def run(self):
        # capture from web cam
        cap = cv2.VideoCapture('http://192.168.1.25:8080/?action=stream')
        while self._run_flag:
            ret, cv_img = cap.read()
            if ret:
                self.change_pixmap_signal.emit(cv_img)
        # shut down capture system
        cap.release()

    def stop(self):
        """Sets run flag to False and waits for thread to finish"""
        self._run_flag = False
        self.wait()


class App(QWidget):
    def __init__(self):
        super().__init__()

        # CREATE WINDOW
        self.setGeometry(500, 100, 400, 300)
        self.setWindowTitle("CARPI V0.0 APP")
        self.video_width = 400
        self.video_height = 400

        # CREATE LABEL TO HOLD IMAGE
        self.image_label = QLabel(self)
        #self.image_label.resize(self.video_width, self.video_height)
        self.image_label.setGeometry(0, 0, self.video_width, self.video_height)

        #SERVO SLIDER
        self.mySlider1 = QSlider(Qt.Horizontal, self)
        self.mySlider1.valueChanged[int].connect(self.changeValue)
        self.mySlider1.setMinimum(0)
        self.mySlider1.setMaximum(255)

        #LED SLIDER
        self.mySlider2 = QSlider(Qt.Horizontal, self)
        self.mySlider2.valueChanged[int].connect(self.changeValue2)
        self.mySlider2.setMinimum(256)
        self.mySlider2.setMaximum(512)

        #CONNECT TO SERVER BUTTON
        self.button = QPushButton("CONNECT", self)
        self.button.clicked.connect(self.clickme)

        # CREATE VERTICAL BOX LAYOUT AND ADD WIDGETS IN ORDER
        vbox = QVBoxLayout()
        vbox.addWidget(self.image_label)
        vbox.addWidget(self.mySlider1)
        vbox.addWidget(self.mySlider2)
        vbox.addWidget(self.button)

        # set the vbox layout as the widgets layout
        self.setLayout(vbox)

        # create the video capture thread
        self.thread = VideoThread()
        # connect its signal to the update_image slot
        self.thread.change_pixmap_signal.connect(self.update_image)
        # start the thread
        self.thread.start()

    def changeValue(self, value):
        print(value)
        x = str(value)
        time.sleep(.01) # add delay for buffer
        s.send(x.encode())  # Encodes and sends message (x)

    def changeValue2(self, value2):
        if value2 > 256:
            print(value2)
            x = str(value2)
            time.sleep(.01)# add delay for buffer
            s.send(x.encode())  # Encodes and sends message (x)

    def clickme(self):
        # printing pressed
        print("pressed")
        s.connect((hostname, port))  # Connects to server


    def closeEvent(self, event):
        self.thread.stop()
        event.accept()

    @pyqtSlot(np.ndarray)
    def update_image(self, cv_img):
        """Updates the image_label with a new opencv image"""
        qt_img = self.convert_cv_qt(cv_img)
        self.image_label.setPixmap(qt_img)

    def convert_cv_qt(self, cv_img):
        """Convert from an opencv image to QPixmap"""
        rgb_image = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
        h, w, ch = rgb_image.shape
        bytes_per_line = ch * w
        convert_to_Qt_format = QtGui.QImage(rgb_image.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
        p = convert_to_Qt_format.scaled(self.video_width, self.video_height, Qt.KeepAspectRatio)
        return QPixmap.fromImage(p)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    a = App()
    a.show()
    sys.exit(app.exec_())