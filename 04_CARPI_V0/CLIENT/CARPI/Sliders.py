import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QSlider, QPushButton
from PyQt5.QtCore import Qt
import socket
import time

#https://www.geeksforgeeks.org/pyqt5-how-to-add-action-to-a-button/

#-----------------SOCKET---------------------
s = socket.socket()
hostname = '192.168.1.50' #Server IP/Hostname
port = 8180 #Server Port
# s.connect((hostname,port)) #Connects to server

class Example(QMainWindow):

    def __init__(self):
        super().__init__()

        #create slider 1
        mySlider = QSlider(Qt.Horizontal, self)
        mySlider.setGeometry(50, 70, 300, 20)
        mySlider.valueChanged[int].connect(self.changeValue)
        mySlider.setMinimum(0)
        mySlider.setMaximum(255)

        #create slider2
        mySlider2 = QSlider(Qt.Horizontal, self)
        mySlider2.setGeometry(50, 120, 300, 20)
        mySlider2.valueChanged[int].connect(self.changeValue2)
        mySlider2.setMinimum(256)
        mySlider2.setMaximum(512)

        # creating a push button
        button = QPushButton("CONNECT", self)
        button.setGeometry(50, 0, 300, 30)
        button.clicked.connect(self.clickme)


        self.setGeometry(500,0,400,200)
        self.setWindowTitle("CAR PI V0.1")
        self.show()



    def changeValue(self, value):
        print(value)
        x = str(value)
        s.send(x.encode())  # Encodes and sends message (x)

    def changeValue2(self, value2):
        if value2 > 256:
            print(value2)
            x = str(value2)
            s.send(x.encode())  # Encodes and sends message (x)

    def clickme(self):
        # printing pressed
        print("pressed")
        s.connect((hostname, port))  # Connects to server
        # s = socket.socket()
        # hostname = '192.168.1.50'  # Server IP/Hostname
        # port = 8180  # Server Port
        # s.connect((hostname, port))  # Connects to server

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())

