import socket

s = socket.socket()
#s = socket()

hostname = '192.168.1.25' #Server IP/Hostname
port = 8180 #Server Port //8180

s.connect((hostname,port)) #Connects to server

while True:

    x = input("Enter message: ") #Gets the message to be sent

    if x == "exit": # closes port
        s.close()
        exit()


    s.send(x.encode())  #Encodes and sends message (x)


