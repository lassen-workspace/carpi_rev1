
#!/usr/bin/env python

#Imports modules
import socket
import RPi.GPIO as GPIO
import time
#--------
# Servo Control
#import time
import wiringpi

# use 'GPIO naming'
wiringpi.wiringPiSetupGpio()

# set #18 to be a PWM output
wiringpi.pinMode(18, wiringpi.GPIO.PWM_OUTPUT)
wiringpi.pinMode(19, wiringpi.GPIO.PWM_OUTPUT)

# set the PWM mode to milliseconds stype
wiringpi.pwmSetMode(wiringpi.GPIO.PWM_MODE_MS)
wiringpi.pwmSetMode(wiringpi.GPIO.PWM_MODE_MS)

# divide down clock
wiringpi.pwmSetClock(192) #192
wiringpi.pwmSetRange(2000) #2000

#---------

listensocket = socket.socket() #Creates an instance of socket
Port = 8180 #Port to host server on
maxConnections = 999
IP = socket.gethostname() #IP address of local machine

listensocket.bind(('',Port))

#Starts server
listensocket.listen(maxConnections)
print("Server started at " + IP + " on port " + str(Port))

#Accepts the incomming connection
(clientsocket, address) = listensocket.accept()
print("New connection made!")

running = True

#Sets up the GPIOs --Can only be used on Raspberry Pi
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

#GPIO.setmode(GPIO.BOARD)
#GPIO.setup(21,GPIO.OUT)

while running:
        message = clientsocket.recv(1024).decode() #Gets the incomming message
        catch = int(message)

        if catch <= 255:
                wiringpi.pwmWrite(18, catch)
                print("SERVO: " + str(catch))

        elif catch > 255 and  catch <= 512 :
                catch2 = catch - 255
                wiringpi.pwmWrite(19, catch2)
                print("LED: " + str(catch2))
#       else:
#               print("out of range =" +str(catch))




